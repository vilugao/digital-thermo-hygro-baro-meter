#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/twi.h>

#define T_F_CPU  1000000
#define adjbyfreq(t)  ((t) / (F_CPU / T_F_CPU))

#include "bmp180.h"

#define makeWord(h, l)  ((unsigned)(h) << 8 | (l))

static bool bmp180_twcall(const unsigned char regaddr)
{
  loop_until_bit_is_clear(TWCR, TWSTO);
  TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_START)
    return false;

  TWDR = BMP180_I2C_ADDR | TW_WRITE;
  TWCR = _BV(TWINT) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_MT_SLA_ACK) {
    if (TW_STATUS == TW_MT_SLA_NACK)
      TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
    return false;
  }

  TWDR = regaddr;
  TWCR = _BV(TWINT) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_MT_DATA_ACK) {
    if (TW_STATUS == TW_MT_DATA_NACK)
      TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
    return false;
  }

  return true;
}

bool bmp180_write(const unsigned char regaddr, const unsigned char data)
{
  if (!bmp180_twcall(regaddr))
    return false;

  TWDR = data;
  TWCR = _BV(TWINT) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_MT_DATA_ACK) {
    if (TW_STATUS == TW_MT_DATA_NACK)
      TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
    return false;
  }

  TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
  return true;
}

bool bmp180_read(
  const unsigned char regaddr,
  unsigned char len,
  unsigned char *data)
{
  if (!bmp180_twcall(regaddr))
    return false;

  TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_REP_START)
    return false;

  TWDR = BMP180_I2C_ADDR | TW_READ;
  TWCR = _BV(TWINT) | _BV(TWEN);
  loop_until_bit_is_set(TWCR, TWINT);
  if (TW_STATUS != TW_MR_SLA_ACK) {
    if (TW_STATUS == TW_MR_SLA_NACK)
      TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
    return false;
  }

  do {
    TWCR = _BV(TWINT) | (--len ? _BV(TWEA) : 0) | _BV(TWEN);
    loop_until_bit_is_set(TWCR, TWINT);
    *data++ = TWDR;
  } while (TW_STATUS == TW_MR_DATA_ACK);
  if (TW_STATUS == TW_MR_DATA_NACK)
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
  return len == 0;
}

bool bmp180_get_calib_param(struct bmp180_calib_param_t *cp)
{
  unsigned char cal[BMP180_CALIB_SIZE];
  if (!bmp180_read(BMP180_CALIB0, BMP180_CALIB_SIZE, cal))
    return false;

  cp->ac1 = makeWord(cal[0], cal[1]);
  cp->ac2 = makeWord(cal[2], cal[3]);
  cp->ac3 = makeWord(cal[4], cal[5]);

  cp->ac4 = makeWord(cal[6], cal[7]);
  cp->ac5 = makeWord(cal[8], cal[9]);
  cp->ac6 = makeWord(cal[10], cal[11]);

  cp->b1 = makeWord(cal[12], cal[13]);
  cp->b2 = makeWord(cal[14], cal[15]);

  cp->mb = makeWord(cal[16], cal[17]);
  cp->mc = makeWord(cal[18], cal[19]);
  cp->md = makeWord(cal[20], cal[21]);
  return true;
}

static bool bmp180_loop_until_sco_clear(void)
{
  unsigned char data;
wait:
  if (!bmp180_read(BMP180_CTRL_MEAS, 1, &data))
    return false;
  if (data & _BV(BMP180_SCO)) {
    _delay_ms(adjbyfreq(1.));
    goto wait;
  }
  return true;
}

bool bmp180_read_ut(unsigned * const ut)
{
  if (!bmp180_loop_until_sco_clear())
    return false;

  unsigned char data[2];
  if (!bmp180_read(BMP180_OUT_MSB, 2, data))
    return false;
  *ut = makeWord(data[0], data[1]);
  return true;
}

int bmp180_get_temperature(
  struct bmp180_calib_param_t * const cp,
  const unsigned long ut)
{
  const long x1 = (ut - cp->ac6) * cp->ac5 >> 15;
  const long x2 = x1 + cp->md;
  if (!x2) /* Do not divide by zero. */
    return 0;
  cp->b5 = x1 + ((long)cp->mc << 11) / x2;
  return (cp->b5 + 8) >> 4;
}

bool bmp180_read_up(
  const enum bmp180_oss oss,
  unsigned long * const up)
{
  if (!bmp180_loop_until_sco_clear())
    return false;

  unsigned char data[3];
  if (!bmp180_read(BMP180_OUT_MSB, 3, data))
    return false;
  *up = ((unsigned long)data[0] << 16 | makeWord(data[1], data[2])) >> (8 - oss);
  return true;
}

unsigned long bmp180_get_pressure(
  const struct bmp180_calib_param_t * const cp,
  const enum bmp180_oss oss,
  const unsigned long up)
{
  const long b6 = cp->b5 - 4000;
  const long b62_12 = b6 * b6 >> 12;

  long x = cp->ac3 * b6 >> 13;
  x += cp->b1 * b62_12 >> 16;
  x = (x + 2) >> 2;
  const unsigned long b4 = cp->ac4 * (unsigned long)(x + 32768) >> 15;
  if (!b4) /* Do not divide by zero. */
    return 0;

  x = cp->b2 * b62_12 >> 11;
  x += cp->ac2 * b6 >> 11;
  const long b3 = (((cp->ac1 * 4L + x) << oss) + 2) >> 2;

  const unsigned long b7 = (up - b3) * (50000 >> oss);
  const long p = b7 < 0x80000000UL ? (b7 << 1) / b4 : b7 / b4 << 1;
  x = p >> 8;
  x = x * x * 3038 >> 16;
  x += p * -7357 >> 16;
  return p + ((x + 3791) >> 4);
}
