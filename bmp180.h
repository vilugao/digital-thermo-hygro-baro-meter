#define BMP180_I2C_ADDR  0xEE

enum bmp180_regaddr {
  BMP180_CALIB0 = 0xAA,
  BMP180_CALIB21 = 0xBF,
  BMP180_CHIP_ID = 0xD0,
  BMP180_VERSION = 0xD1,
  BMP180_SOFT_RESET = 0xE0,
  BMP180_CTRL_MEAS = 0xF4,
  BMP180_OUT_MSB = 0xF6,
  BMP180_OUT_LSB,
  BMP180_OUT_XLSB,
};
#define BMP180_CALIB_SIZE (BMP180_CALIB21 - BMP180_CALIB0 + 1)

#define BMP180_SCO  5
#define BMP180_OSS  6

#define BMP180_T_MEASURE  0x2E
#define BMP180_P_MEASURE(oss)  ((oss) << BMP180_OSS | 0x34)

enum bmp180_oss {
  BMP180_ULPWR,
  BMP180_STD,
  BMP180_HRES,
  BMP180_UHRES,
};

/* -- Low level I/O calls -- */

_Bool bmp180_write(unsigned char addr, unsigned char data);
_Bool bmp180_read(unsigned char addr, unsigned char len, unsigned char *data);

/* -- High level calls -- */

struct bmp180_calib_param_t {
  short ac1, ac2, ac3;
  unsigned short ac4, ac5, ac6;
  short b1, b2;
  short mb, mc, md;

  long b5;
};

_Bool bmp180_get_calib_param(struct bmp180_calib_param_t *);

#define bmp180_measure_t() \
  bmp180_write(BMP180_CTRL_MEAS, BMP180_T_MEASURE)
#define bmp180_measure_p(oss) \
  bmp180_write(BMP180_CTRL_MEAS, BMP180_P_MEASURE(oss))

#define BMP180_T_WAIT_TYP_MS  3
#define BMP180_T_WAIT_MAX_MS  4.5
#define BMP180_P_WAIT_TYP_MS(oss)  ((2 << (oss)) + 1)
#define BMP180_P_WAIT_MAX_MS(oss)  ((3 << (oss)) + 1.5)

_Bool bmp180_read_ut(unsigned *ut);
_Bool bmp180_read_up(const enum bmp180_oss oss, unsigned long *up);

int bmp180_get_temperature(struct bmp180_calib_param_t *, unsigned long ut);
unsigned long bmp180_get_pressure(const struct bmp180_calib_param_t *,
                                  enum bmp180_oss, unsigned long up);

/* -- Pressure conversions -- */

#ifdef M_E /* require <math.h> */
#define STD_T0  288.15f
#define STD_L0  -.0065f
#define STD_P0  101325

static inline int bmp180_to_altitude(const unsigned long p)
{
  return lrintf(STD_T0 / STD_L0 * (powf((1.f / STD_P0) * p, 1 / 5.2558f) - 1));
}

static inline float bmp180_tomslfactor(const float height_m)
{
  return powf(fmaf(height_m, STD_L0 / STD_T0, 1), -5.2558f);
}

#undef STD_T0
#undef STD_L0
#undef STD_P0
#endif
