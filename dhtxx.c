#include <avr/io.h>

#include "dhtxx.h"

#define T_F_CPU  1000000

enum dht_status dht_read(
  const unsigned char pin, const unsigned char dhttype,
  unsigned char * const rh, int * const t)
{
  TCCR0A = 0;
#if T_F_CPU == 16000000 || T_F_CPU == 8000000
  TCCR0B = _BV(CS01); /* clk/8, TOV = 128 us @ 16 MHz, 256 us @ 8 MHz */
#elif T_F_CPU == 1000000
  TCCR0B = _BV(CS00); /* clk/1, TOV = 256 us @ 1 MHz */
#else
# error "Set the T_F_CPU to 16MHz, 8MHz or 1MHz!"
#endif

  /* Host release a start signal */
  DHT_DDR &= ~_BV(pin);
  TCNT0 = 0;
  TIFR0 |= _BV(TOV0);
  while (bit_is_clear(DHT_PIN, pin))
    if (bit_is_set(TIFR0, TOV0))
      goto timedout;

  /* Wait for response signal */
  while (bit_is_set(DHT_PIN, pin))
    if (bit_is_set(TIFR0, TOV0))
      goto timedout;
  loop_until_bit_is_set(DHT_PIN, pin);

  /* Pulled ready output */
  loop_until_bit_is_clear(DHT_PIN, pin);
  TCNT0 = 0;
  TIFR0 |= _BV(TOV0);

  /* Receiving data */
  uint8_t buf[5];
  for (unsigned char i = 0; i < 5; i++) {
    uint8_t val = 0;
    for (unsigned char b = 8; b > 0; b--) {
      loop_until_bit_is_set(DHT_PIN, pin);
      const uint8_t cntcycle = 2 * TCNT0;
      loop_until_bit_is_clear(DHT_PIN, pin);
      const uint8_t cnt = TCNT0;
      TCNT0 = 0;
      if (bit_is_set(TIFR0, TOV0))
        goto timedout;

      val <<= 1;
      if (cnt > cntcycle)
        val |= 1;
    }
    buf[i] = val;
    val = 0;
  }
  TCCR0B = 0;

  if ((uint8_t)(buf[0] + buf[1] + buf[2] + buf[3]) != buf[4])
    return DHT_ECHKSUM;

  if (dhttype == 22) {
    *rh = ((buf[0] << 8) + buf[1]) / 10;
    *t = (buf[2] << 8) + buf[3];
  } else {
    *rh = buf[0];
    *t = buf[2] * 10 + (buf[3] & 0x0F);
  }
  return DHT_OK;

timedout:
  TCCR0B = 0;
  return DHT_ETIMEOUT;
}
