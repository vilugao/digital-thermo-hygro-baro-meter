/* Pin definitions */
#define DHT_DDR   DDRC
#define DHT_PIN   PINC
#define DHT_P     PC3

/* === */

enum dht_status {
  DHT_OK,
  DHT_ETIMEOUT,
  DHT_ECHKSUM,
};

#define dht_start_measure(pin)  (DHT_DDR |= _BV(pin))
#define DHT22_WAIT_MS  .5
#define DHT11_WAIT_MS  18
enum dht_status dht_read(unsigned char pin, unsigned char dhttype,
                         unsigned char *rh, int *t);
