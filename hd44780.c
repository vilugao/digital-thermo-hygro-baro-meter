#include <avr/io.h>
#include <util/delay.h>

#include "hd44780.h"

#define T_F_CPU  1000000
#define adjbyfreq(t)  ((t) / (F_CPU / T_F_CPU))


#if T_F_CPU <= 2000000
# define wait0_5us()  (void)0
#else
# define wait0_5us()  _delay_us(adjbyfreq(.5))
#endif

static void _hd44780_outnibble(const unsigned char n)
{
  HD44780_CTRL_PORT |= _BV(HD44780_E);
  HD44780_DATA_PORT = (HD44780_DATA_PORT & ~(0xF << HD44780_D4)) | n;
  wait0_5us();
  HD44780_CTRL_PORT &= ~_BV(HD44780_E);
  wait0_5us();
}
#define hd44780_outnibble(x) \
  _hd44780_outnibble(((x) & 0xF) << HD44780_D4)

void hd44780_outbyte(const unsigned char b)
{
  hd44780_outnibble(b >> 4);
  hd44780_outnibble(b);
  _delay_us(adjbyfreq(37.));
}


void hd44780_clear(void)
{
  hd44780_outbyte(1);
  _delay_ms(adjbyfreq(1.52));
}

void hd44780_home(void)
{
  hd44780_outbyte(2);
  _delay_ms(adjbyfreq(1.52));
}


#if HD44780_FONT5X11
# define HD44780_FNSET_4BIT  0x24
#else
# define HD44780_FNSET_4BIT  0x20
#endif
#define HD44780_FNSET_8BIT  0x30

void hd44780_init(const _Bool twolines)
{
#if 1 //(#HD44780_CTRL_DDR) == (#HD44780_DATA_DDR)
  HD44780_DATA_DDR |= _BV(HD44780_RS) | _BV(HD44780_E) | (0xF << HD44780_D4);
#else
  HD44780_CTRL_DDR |= _BV(HD44780_RS);
  HD44780_CTRL_DDR |= _BV(HD44780_E);
  HD44780_DATA_DDR |= 0xF << HD44780_D4;
#endif

  _delay_ms(adjbyfreq(40.));
  hd44780_setcmd();

  hd44780_outnibble(HD44780_FNSET_8BIT >> 4);
  _delay_ms(adjbyfreq(4.1));
  hd44780_outnibble(HD44780_FNSET_8BIT >> 4);
  _delay_us(adjbyfreq(100.));
  hd44780_outnibble(HD44780_FNSET_8BIT >> 4);
  _delay_us(adjbyfreq(37.));

  hd44780_outnibble(HD44780_FNSET_4BIT >> 4);
  _delay_us(adjbyfreq(37.));

  hd44780_outbyte(HD44780_FNSET_4BIT | (twolines ? 8 : 0) | (HD44780_FONT5X11 ? 4 : 0));
}
