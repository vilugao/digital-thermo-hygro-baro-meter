/* Pin definitions */
#define HD44780_CTRL_DDR   DDRD
#define HD44780_CTRL_PORT  PORTD
#define HD44780_RS         PD2
#define HD44780_E          PD3

#define HD44780_DATA_DDR   DDRD
#define HD44780_DATA_PORT  PORTD
#define HD44780_D4         PD4
/* End pin definitions */

#define HD44780_FONT5X11  0

/* === */

#define hd44780_setcmd()  (HD44780_CTRL_PORT &= ~_BV(HD44780_RS))
#define hd44780_setdata()  (HD44780_CTRL_PORT |= _BV(HD44780_RS))
void hd44780_outbyte(unsigned char);

void hd44780_init(_Bool twolines);
void hd44780_clear(void);
void hd44780_home(void);

/* Commands */

#define HD44780_ENTRYMODE_DEC        0x04
#define HD44780_ENTRYMODE_DEC_SHIFT  0x05
#define HD44780_ENTRYMODE_INC        0x06
#define HD44780_ENTRYMODE_INC_SHIFT  0x07

#define HD44780_DISP_OFF        0b1000
#define HD44780_DISP_ON         0b1100
#define HD44780_DISP_CUR        0b1110
#define HD44780_DISP_CUR_BLINK  0b1111

#define HD44780_SHIFT_CUR_L   0x10
#define HD44780_SHIFT_CUR_R   0x14
#define HD44780_SHIFT_DISP_L  0x18
#define HD44780_SHIFT_DISP_R  0x1C

#define HD44780_CGADDR(i)  (0x40 | ((i) & 7) << 3)

#define HD44780_CUR_AT(l, x)  (((l) ? 0xC0 : 0x80) | (x))
