#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <math.h>

/*---*---*/

/* Charset pre-programmed in the LCD character ROM. */
#define HD44780_CHARSET_ROM  0x0A

/*
  Your local altitude in metres, for displaying the sea level pressure.
  Comment this to disable the sea level pressure calculation and display.
*/
//#define ALTITUDE_METRES  760
/* Just naming changes: 0 = SP/MSL; 1 = QFE/QNH. */
#define USE_QFE_QNH  0

/* Disable dew point calculation and display. */
//#define NO_DEWPOINT

/* Use NTC temperature sensing (0 = disable, 1 = enable). */
#define USE_NTC  0

/* 1 = Enable Watchdog; 0 = disable. */
#define USE_WATCHDOG  0

/*---*---*/

#include "bmp180.h"
#include "dhtxx.h"
#if USE_NTC
#include "ntc.h"
#endif
#include "hd44780.h"
#include "nmeaout.h"

#define T_F_CPU  1000000
#define adjbyfreq(t)  ((float)(t) / (F_CPU / T_F_CPU))

#define LED_BUILTIN_PORT  PORTB
#define LED_BUILTIN_DDR  DDRB
#define LED_BUILTIN  PB5

static struct bmp180_calib_param_t bmp180_calib_param;

static struct {
  char t_c;
  unsigned char rh_p;
#ifndef NO_DEWPOINT
  char dpt_c;
#endif
  unsigned short qfe_hPa;
#ifdef ALTITUDE_METRES
  unsigned short qnh_hPa;
#endif
} weather_data;

static unsigned char timercnt;

/* --- POLLING SENSORS AND SENDING BY UART --- */

static void poll(void)
{
  int sumtemp = 0;
  uint8_t count = 0;

  struct {
    int t;
    unsigned long p;
  } bmp;

  weather_data.rh_p = 0;
  weather_data.qfe_hPa = 0;

#if 1
  /* Only with DHT11, and not using oss = BMP180_UHRES. */
  dht_start_measure(DHT_P);
  OCR1A = TCNT1 + (unsigned)(DHT11_WAIT_MS * 1e-3 * T_F_CPU / 64);
  TIFR1 |= _BV(OCF1A);
#endif

  bmp.p = 0;
  if (bmp180_calib_param.ac5) {
    if (!bmp180_measure_t())
      goto after_bmp180;
    _delay_ms(adjbyfreq(BMP180_T_WAIT_TYP_MS));
    unsigned ut;
    if (!bmp180_read_ut(&ut))
      goto after_bmp180;

    if (!bmp180_measure_p(BMP180_STD))
      goto after_bmp180;
    _delay_ms(adjbyfreq(BMP180_P_WAIT_TYP_MS(BMP180_STD)));
    unsigned long up;
    if (!bmp180_read_up(BMP180_STD, &up))
      goto after_bmp180;

    bmp.t = bmp180_get_temperature(&bmp180_calib_param, ut);
    bmp.p = bmp180_get_pressure(&bmp180_calib_param, BMP180_STD, up);
  }
after_bmp180:

#if 0
  /* With DHT22 or using oss = BMP180_UHRES. */
  dht_start_measure(DHT_P);
  OCR1A = TCNT1 + (unsigned)(DHT22_WAIT_MS * 1e-3 * T_F_CPU / 64);
  TIFR1 |= _BV(OCF1A);
#endif

  wdt_reset();

  {
    struct {
      int t;
      unsigned char rh;
    } dht;

    loop_until_bit_is_set(TIFR1, OCF1A);
    if (dht_read(DHT_P, 11, &dht.rh, &dht.t) == DHT_OK) {
      nmea_startxdr();
      nmeaxdr_senddata('H', dht.rh, 0, 'P', PSTR("DHTrh"));
      nmeaxdr_senddata('C', dht.t, 1, 'C', PSTR("DHTtemp"));
      nmea_end();

      weather_data.rh_p = dht.rh;
      sumtemp += dht.t;
      count++;
    }
  }

  wdt_reset();

  if (bmp.p) {
    nmea_startxdr();
    nmeaxdr_senddata('C', bmp.t, 1, 'C', PSTR("BMPtemp"));
    nmeaxdr_senddata('P', (bmp.p + 5) / 10, 4, 'B', PSTR("BMPbaro"));
    nmea_end();

    weather_data.qfe_hPa = (bmp.p + 50) / 100;
#ifdef ALTITUDE_METRES
    weather_data.qnh_hPa =
      lrintf(bmp180_tomslfactor(ALTITUDE_METRES) / 100 * bmp.p);
#endif
    sumtemp += bmp.t;
    count++;
  }

  wdt_reset();

#if USE_NTC
  {
    const int t = ntc_readtemp();
    if (t > -400) {
      nmea_startxdr();
      nmeaxdr_senddata('C', t, 1, 'C', PSTR("NTC"));
      nmea_end();

      sumtemp += t;
      count++;
    }
  }
#endif

  {
    const float t = sumtemp / (float)(10 * count);
#ifndef NO_DEWPOINT
    if (weather_data.rh_p) {
      /* Alduchov & Eskridge constant set */
      const float B = 17.625f, C = 243.04f;
      const float g = B * t / (t + C) + logf(weather_data.rh_p * .01f);
      weather_data.dpt_c = lrintf(C * g / (B - g));
    }
#endif
    weather_data.t_c = lrintf(t);
  }
}

/* --- DISPLAYING TO LCD --- */

static void lcd_putc(char c)
{
  if (HD44780_FONT5X11 && (
        HD44780_CHARSET_ROM == 0xA00 ||
        HD44780_CHARSET_ROM == 0x0A ||
        HD44780_CHARSET_ROM == 0x0E))
    /* Change the char with descenders to its full-height 5x10 forms */
    switch (c) {
      case 'g': case 'j':
      case 'p': case 'q': case 'y':
        c |= 0x80;
        break;
    }
  hd44780_outbyte(c);
}

static void lcd_puts_P(PGM_P s)
{
  char c;
  hd44780_setdata();
  while ((c = pgm_read_byte(s++)) != '\0')
    lcd_putc(c);
}

static void lcd_init(void)
{
  hd44780_init(1);
  hd44780_outbyte(HD44780_ENTRYMODE_INC);

#if (HD44780_CHARSET_ROM == 0xA02) /* For HD44780U-A02 */
# define HD44780_CHR_ATILDE "\xE3"
# define HD44780_CHR_DEG "\xB0"
#elif (HD44780_CHARSET_ROM == 0x0B) /* For ST7066U-0B */
# define HD44780_CHR_ATILDE "\xAB"
# define HD44780_CHR_DEG "\xB2"
#elif (HD44780_CHARSET_ROM == 0x0E) /* For ST7066U-0E */
# define HD44780_CHR_ATILDE "\xCB"
# define HD44780_CHR_DEG "\xD2"
#else
  /* Set a LATIN SMALL LETTER A WITH TILDE to third CGRAM character. */
  hd44780_outbyte(HD44780_CGADDR(3));
  lcd_puts_P(PSTR("\x0D\x12\x0E\x01\x0F\x11\x0F\x80"));
# define HD44780_CHR_ATILDE "\x03"
# define HD44780_CHR_DEG "\xDF"
#endif

  hd44780_setcmd();
  hd44780_outbyte(HD44780_DISP_ON);
}

static void lcd_putd(char c)
{
  /* Change zero digit to remove the stylish slashed zero. */
  if (c == '0')
    c = 'O';
  hd44780_outbyte(c);
}

static void lcd_printnum(const char num)
{
  hd44780_setdata();
  const unsigned char absnum = num < 0 ? -num : num;
  const unsigned char q = (absnum * 205) >> 11;
  lcd_putd(absnum - 10 * q + '0');
  lcd_putd(q ? q + '0' : num < 0 ? '-' : ' ');
}

static void lcd_print_hPa(unsigned p)
{
  hd44780_setdata();
#if 0
  do {
    lcd_putd(p % 10 + '0');
    p /= 10;
  } while (p);
#else
  // Ref.: http://homepage.divms.uiowa.edu/~jones/bcd/decimal.html
  unsigned char d[5];

  d[1] = (p >> 4) & 0xF;
  d[2] = (p >> 8) & 0xF;
  d[3] = (p >> 12) & 0xF;

  d[0] = 6 * (d[3] + d[2] + d[1]) + (p & 0xF);
  d[4] = (d[0] * 205) >> 11;
  lcd_putd(d[0] - 10 * d[4] + '0');

  d[1] = d[4] + 9 * d[3] + 5 * d[2] + d[1];
  d[4] = (d[1] * 205) >> 11;
  lcd_putd(d[1] - 10 * d[4] + '0');

  d[2] = d[4] + 2 * d[2];
  d[4] = (d[2] * 26) >> 8;
  lcd_putd(d[2] - 10 * d[4] + '0');

  d[3] = d[4] + 4 * d[3];
  if (d[3])
    lcd_putd(d[3] + '0');
#endif
}

static const char celsius_unit[] PROGMEM = HD44780_CHR_DEG "C";
static const char three_bars[] PROGMEM = "---";

/*
  +;....,....;....,+
  | 25 °C  RH 50 % |
  |  SP  1010 hPa  |
  +;....,....;....,+
*/
static void display(void)
{
  hd44780_setcmd();
  hd44780_clear();
  hd44780_outbyte(HD44780_ENTRYMODE_INC);

  hd44780_outbyte(HD44780_CUR_AT(0, 4));
  lcd_puts_P(celsius_unit);

#ifndef NO_DEWPOINT
  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(0, 8));
  lcd_puts_P(PSTR("RH"));
#endif

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(0, 14));
  hd44780_setdata();
  lcd_putc('%');

#ifdef ALTITUDE_METRES
  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(1, 2));
  lcd_puts_P(USE_QFE_QNH ? PSTR("QFE") : PSTR("SP "));
#endif

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(1, 11));
  lcd_puts_P(PSTR("hPa"));


  hd44780_setcmd();
  hd44780_outbyte(HD44780_ENTRYMODE_DEC);

  hd44780_outbyte(HD44780_CUR_AT(0, 2));
  lcd_printnum(weather_data.t_c);

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(0, 12));
  if (weather_data.rh_p)
    lcd_printnum(weather_data.rh_p);
  else
    lcd_puts_P(three_bars);

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(1, 9));
  if (weather_data.qfe_hPa)
    lcd_print_hPa(weather_data.qfe_hPa);
  else
    lcd_puts_P(three_bars);
}

/*
  +;....,....;....,+
  | 25 °C  DP 20 °C|
  |  MSL 1013 hPa  |
  +;....,....;....,+
*/
static void displaysecondview(void)
{
#ifndef NO_DEWPOINT
  hd44780_setcmd();
  hd44780_outbyte(HD44780_ENTRYMODE_INC);
  hd44780_outbyte(HD44780_CUR_AT(0, 8));
  lcd_puts_P(PSTR("DP"));

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(0, 14));
  lcd_puts_P(celsius_unit);

  if (weather_data.rh_p) {
    hd44780_setcmd();
    hd44780_outbyte(HD44780_ENTRYMODE_DEC);
    hd44780_outbyte(HD44780_CUR_AT(0, 12));
    lcd_printnum(weather_data.dpt_c);
  }
#endif

#ifdef ALTITUDE_METRES
  hd44780_setcmd();
  hd44780_outbyte(HD44780_ENTRYMODE_INC);
  hd44780_outbyte(HD44780_CUR_AT(1, 2));
  lcd_puts_P(USE_QFE_QNH ? PSTR("QNH") : PSTR("MSL"));

  if (weather_data.qfe_hPa) {
    hd44780_setcmd();
    hd44780_outbyte(HD44780_ENTRYMODE_DEC);
    hd44780_outbyte(HD44780_CUR_AT(1, 9));
    lcd_print_hPa(weather_data.qnh_hPa);
  }
#endif
}

/* --- ENTRY LOOP --- */

static void ioinit(void)
{
  /* Turn off the unused fuctions and ports */
  PRR = _BV(PRTIM2) | _BV(PRSPI) | (USE_NTC ? 0 : _BV(PRADC));

  /* Disable unused fuctions and ports */
  DIDR0 = _BV(ADC0D) | _BV(ADC1D) | _BV(ADC2D);
  DIDR1 = _BV(AIN0D) | _BV(AIN1D);
  ACSR = _BV(ACD);

  /* Pull-up unused pins */
  PORTB = _BV(PB3) | _BV(PB4);
  PORTD = _BV(PD2) | _BV(PD3);

  /* Initialize the LED_BUILTIN */
  LED_BUILTIN_DDR |= _BV(LED_BUILTIN);

#if F_CPU >= 8000000
  CLKPR = _BV(CLKPCE);
# if F_CPU == 16000000
  CLKPR = _BV(CLKPS2); /* clk/16 */
# elif F_CPU == 8000000
  CLKPR = _BV(CLKPS1) | _BV(CLKPS0); /* clk/8 */
# else
#  error "F_CPU not 8 MHz or 16 MHz!"
# endif
#endif

  TCCR1A = 0; /* Normal operation timer */
  //TCCR1B = _BV(CS12) | _BV(CS10); /* clk/1024; TOV = 4,19 s @ 16 MHz */
  TCCR1B = _BV(CS11) | _BV(CS10); /* clk/64; TOV = 4,19 s @ 1 MHz */

  lcd_init();
  uart_init();
#if T_F_CPU > 6400000
  TWBR = (T_F_CPU / 400000 - 16) / 2 + 1; /* TWI freq. = 400 kHz (Fast mode) */
#endif
#if USE_NTC
  ntc_setup();
#endif
}

static void slow_puts_P(PGM_P s)
{
  char c;
  hd44780_setdata();
  while ((c = pgm_read_byte(s++)) != '\0') {
    uart_putc(c);
    lcd_putc(c);
    _delay_ms(adjbyfreq(50.));
  }
}

static void display_intro(void)
{
  static const char intro1[] PROGMEM = "Digital thermo/";
  static const char intro2[] PROGMEM = "hygro/barometer";
  static const char intro3[] PROGMEM = "by Vi Lug" HD44780_CHR_ATILDE "o";
  static const char intro4[] PROGMEM = "V.: " __DATE__; /* "MMM DD YYYY" */

  TCNT1 = 16384;
  TIFR1 |= _BV(TOV1);

  hd44780_setcmd();
  hd44780_clear();
  slow_puts_P(intro1);

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(1, 1));
  slow_puts_P(intro2);

  loop_until_bit_is_set(TIFR1, TOV1);

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(0, 17 + 2));
  lcd_puts_P(intro3);
  uart_puts_P(PSTR(" by Vi Lugão - "));

  hd44780_setcmd();
  hd44780_outbyte(HD44780_CUR_AT(1, 17 + 1));
  lcd_puts_P(intro4);
  uart_puts_P(intro4);

  hd44780_setcmd();
  for (unsigned char i = 17; i > 0; i--) {
    hd44780_outbyte(HD44780_SHIFT_DISP_L);
    _delay_ms(adjbyfreq(100.));
  }

  uart_putc('\r');
  uart_putc('\n');
}

#define WDE_8S    (_BV(WDE) | _BV(WDP3) | _BV(WDP0))
#define WDE_64MS  (_BV(WDE) | _BV(WDP1))

int main(void)
{
  wdt_reset();
  if (!USE_WATCHDOG)
    MCUSR &= ~_BV(WDRF);
  WDTCSR |= _BV(WDCE) | _BV(WDE);
  WDTCSR = USE_WATCHDOG ? WDE_8S : 0;

  ioinit();
  display_intro();
  bmp180_get_calib_param(&bmp180_calib_param); /* Comment this line if you don't use BMP180. */

  TIMSK1 = _BV(TOIE1);
  SMCR = _BV(SE);
  while (1) {
    TIFR1 |= _BV(TOV1);
    sei();
    sleep_cpu();
    cli();
    if (USE_WATCHDOG) {
      wdt_reset();
      WDTCSR |= _BV(WDCE) | _BV(WDE);
      WDTCSR = _BV(WDE) | WDE_64MS;
    }

    LED_BUILTIN_PORT |= _BV(LED_BUILTIN);
    if ((timercnt & 1) == 0) {
      if ((timercnt & 2) == 0)
        poll();
      display();
    } else {
      displaysecondview();
    }
    timercnt++;
    LED_BUILTIN_PORT &= ~_BV(LED_BUILTIN);
#if 0 /* debug timer */
    uart_putc(':');
    uart_putu(TCNT1, 0);
    uart_putc('\r');
    uart_putc('\n');
#endif
    if (USE_WATCHDOG) {
      cli();
      wdt_reset();
      WDTCSR |= _BV(WDCE) | _BV(WDE);
      WDTCSR = WDE_8S;
    }
  }

  return 0;
}

EMPTY_INTERRUPT(TIMER1_OVF_vect);
