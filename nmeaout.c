#include <avr/pgmspace.h>

#include "nmeaout.h"

static unsigned char nmea_chksum;

void uart_init(void)
{
#define BAUD (9600 * (F_CPU / 1000000))
#include <util/setbaud.h>
  UBRR0 = UBRR_VALUE;
#if USE_2X
  UCSR0A |= _BV(U2X0);
#else
  UCSR0A &= ~_BV(U2X0);
#endif
  UCSR0B = _BV(TXEN0);
  UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
}

void uart_putc(const char c)
{
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
  nmea_chksum ^= c;
}

void uart_puts_P(PGM_P s)
{
  char c;
  while ((c = pgm_read_byte(s++)) != '\0')
    uart_putc(c);
}

void uart_putu(unsigned val, const char dec)
{
  const char digit = val % 10 + '0';
  val /= 10;
  if (val || dec > 0)
    uart_putu(val, dec - 1);
  if (dec == 1)
    uart_putc('.');
  uart_putc(digit);
}


void nmea_startxdr(void)
{
  nmea_chksum = '$';
  uart_puts_P(PSTR("$WIXDR"));
}

void nmeaxdr_senddata(
  const char type,
  int val, const char dec,
  const char unit, PGM_P name)
{
  uart_putc(',');
  uart_putc(type);
  uart_putc(',');
  if (val < 0) {
    uart_putc('-');
    val = -val;
  }
  uart_putu(val, dec);
  uart_putc(',');
  uart_putc(unit);
  uart_putc(',');
  uart_puts_P(name);
}

static char nbtohex(unsigned char nb)
{
  nb |= '0';
  if (nb > '9')
    nb += 'A' - '0' - 10;
  return nb;
}

static void uart_puthex(const unsigned char x)
{
  uart_putc(nbtohex(x >> 4));
  uart_putc(nbtohex(x & 0xF));
}

void nmea_end(void)
{
  const unsigned char s = nmea_chksum;
  uart_putc('*');
  uart_puthex(s);
  uart_putc('\r');
  uart_putc('\n');
}
