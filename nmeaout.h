void uart_init(void);
void uart_putc(char);
void uart_puts_P(PGM_P);
void uart_putu(unsigned val, char dec);

void nmea_startxdr(void);
void nmeaxdr_senddata(char type, int val, char dec,
                      char unit, PGM_P name);
void nmea_end(void);
