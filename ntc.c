#include <math.h>
#include <avr/io.h>

#include "ntc.h"

/* Resistance values in decaohm */
#define NTC_RLOAD 1000
/* Default values obtained from MF52 10K datasheet (Beta = 3950 K) */
#define NTC_R0 3212
#define NTC_R25 1000
#define NTC_R50 359

void ntc_setup(void)
{
  ADMUX = _BV(REFS0) | (NTC_ADC_CH << MUX0);
}

static unsigned int ntc_getadcval(void)
{
  NTC_DDR |= _BV(NTC_OUT2GND);
  ADCSRA = _BV(ADEN) | _BV(ADSC) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
  loop_until_bit_is_clear(ADCSRA, ADSC);
  NTC_DDR &= ~_BV(NTC_OUT2GND);
  return ADCW;
}

#define R2VAL(r)  ((r) / ((r) + (double)NTC_RLOAD))

static inline int t_l3(const double adc)
{
  const double tc1 = 250 / (
                       (R2VAL(NTC_R25) - R2VAL(NTC_R0))
                       * (R2VAL(NTC_R25) - R2VAL(NTC_R50))
                     );
  const double tc2 = 500 / (
                       (R2VAL(NTC_R50) - R2VAL(NTC_R0))
                       * (R2VAL(NTC_R50) - R2VAL(NTC_R25))
                     );

  const double adc_mval0 = adc - R2VAL(NTC_R0);
  return lrint(tc1 * adc_mval0 * (adc - R2VAL(NTC_R50))
               + tc2 * adc_mval0 * (adc - R2VAL(NTC_R25)));
}

int ntc_readtemp(void)
{
  const unsigned adcval = ntc_getadcval();
  if (adcval == 0)
    return ~(~0u >> 1);
  return t_l3(adcval * (1. / 1023));
}
