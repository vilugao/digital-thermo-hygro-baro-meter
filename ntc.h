/* SCHEMATIC SETUP:

  +-[10K]-+-[NTC]-+
  |       |       |
  AVcc    A1      A2
*/

#define NTC_ADC_CH  1

#define NTC_DDR  DDRC
#define NTC_OUT2GND  PC2

void ntc_setup(void);
int ntc_readtemp(void);
